# beamerthemegrandshell

Grandshell LaTeX Beamer Theme

![](https://raw.github.com/steckerhalter/beamerthemegrandshell/master/example/grandshell-0.jpg)
![](https://raw.github.com/steckerhalter/beamerthemegrandshell/master/example/grandshell-1.jpg)

The [example](example/) folder contains a sample Org Beamer document that generated these images.

## Requirements

### LaTeX packages

- hyperref
- minted
- tikz

### Fonts

Bera fonts (part of `texlive-fonts-extra` in Debian)

## Usage

With TeXLive it should work if you put `beamerthemegrandshell.sty` into the folder: `~/texmf/tex/latex`

After that in your `LaTeX` beamer file use it with:

```latex
\usetheme{grandshell}
```

or with Emacs Org mode in the header:

```org
#+BEAMER_THEME: grandshell
```
